package ganzlustig.attendancerecsim

import org.junit.Test
import org.slf4j.LoggerFactory

class SimulationTest {

    private val log = LoggerFactory.getLogger(this.javaClass)

    @Test
    fun testSimulation() {

        log.info("Sample simulation with Jane and John ...")

        val seed = System.currentTimeMillis()
        val perfectEnding = true
        val sim = Simulation(Worker.createSamples(), perfectEnding, durationFromHours(WORK_DAY_HOURS), seed)
        /*val simRes = */sim.call()

//        writeICal(simRes, iCalOutput)
//        writeWorkShiftsCsv(simRes, csvOutputWorkShifts)
//        writeDayStatesCsv(simRes, csvOutputDayStates)
    }
}
