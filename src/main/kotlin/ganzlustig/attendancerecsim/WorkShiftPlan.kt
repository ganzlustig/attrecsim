package ganzlustig.attendancerecsim

import kotlinx.serialization.Serializable
import java.time.DayOfWeek
import java.time.Duration
import java.time.LocalTime

@Serializable
data class WorkShiftPlan(val start: WeekDayTime, val end: WeekDayTime) {

    fun duration() = end.minus(start)

    constructor(start: WeekDayTime, duration: Duration)
            : this(start, start.plus(duration))

    constructor(day: DayOfWeek, hour: Int, minute: Int, durationHours: Int)
            : this(WeekDayTime(day, LocalTime.of(hour, minute)), Duration.ofHours(durationHours.toLong()))


    fun serialize() : List<String> {
        return listOf(
            start.serialize().foldWithDelim("_"),
            start.serialize().foldWithDelim("_"))
    }

    companion object {
        fun parse(parts: List<String>) : WorkShiftPlan {
            return WorkShiftPlan(
                WeekDayTime.parse(parts[0].splitWithSpace("_")),
                WeekDayTime.parse(parts[1].splitWithSpace("_")))
        }
    }
}
