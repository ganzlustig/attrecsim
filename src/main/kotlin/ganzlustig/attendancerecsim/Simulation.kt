package ganzlustig.attendancerecsim

import net.fortuna.ical4j.model.TextList
import net.fortuna.ical4j.model.component.VEvent
import net.fortuna.ical4j.model.parameter.Cn
import net.fortuna.ical4j.model.parameter.Role
import net.fortuna.ical4j.model.property.Attendee
import net.fortuna.ical4j.model.property.Categories
import org.slf4j.LoggerFactory
import java.lang.IllegalStateException
import java.net.URI
import java.time.DayOfWeek
import java.time.Duration
import java.time.LocalDate
import java.time.LocalTime
import java.util.*
import java.util.concurrent.Callable
import kotlin.collections.HashMap
import kotlin.math.*

const val PERFECT_TIME_UPPER_HOURS = 1.5

enum class DayState {
    WEEK_DAY,
    WORK,
    WEEK_END,
    SICK,
    VACATION,
    HOLIDAY;
}
typealias WorkShifts = List<WorkShift>
typealias DayStates = Map<LocalDate, Set<DayState>>

/**
 * Even a completely unsteady worker would not come/leave
 * more then this amount of hours off schedule.
 */
const val COMMON_HOURS_OFF_SCHEDULE = 1.5

/**
 * Generally, people would more likely come late
 * (positive value in fraction of an hour) then early.
 */
const val LATENESS_BIAS = 0.25

/**
 * If sick one day, how likely is one to be sick the next day.
 */
const val SICKNESS_CONTINUATION_LIKELIHOOD = 0.5

/**
 * If on vacation one day, how likely is one to be on vacation the next day.
 */
const val VACATION_CONTINUATION_LIKELIHOOD = 0.80

/**
 * The maximum hours one might work per day (this is mandated by German law).
 */
const val MAX_WORK_DAY_HOURS = 10.0

data class SimulationResult(
    val worker: Worker,
    val workShifts: WorkShifts = emptyList(),
    val dayStates: DayStates = emptyMap())
{
    fun combineWith(other: SimulationResult) : SimulationResult {

        if (worker != other.worker) {
            throw IllegalStateException("Can only combine ${this.javaClass.canonicalName}s of the same worker")
        }

        val newWorkShifts = workShifts.toMutableList()
        newWorkShifts.addAll(other.workShifts)

        val newDayStates = dayStates.toMutableMap()
        newDayStates.putAll(other.dayStates)

        return SimulationResult(worker, newWorkShifts, newDayStates)
    }
}

data class SimulationResults(
    val workerResults: Map<Worker, SimulationResult>)

/**
 * @param workers the workers to simulate clock-in and -out time-stamps for.
 * @param seed random seed used for the whole simulation.
 */
class Simulation(
    private val workers: List<Worker>,
    private val perfectEnding: Boolean,
    private val workDay: Duration = durationFromHours(WORK_DAY_HOURS),
    private val seed: Long = 0L)
    : Callable<SimulationResults>
{
    private val random = Random(seed)

    private val log = LoggerFactory.getLogger(this.javaClass)

    companion object {
        fun convertToCalendar(simRes: SimulationResults, timeZone: TimeZone = TimeZone.getTimeZone("GMT+1"))
                : net.fortuna.ical4j.model.Calendar
        {
            val cal = net.fortuna.ical4j.model.Calendar()

            for (workerWorkTimes in simRes.workerResults) {
                val worker = workerWorkTimes.key
                val workerAtt = Attendee(URI.create("mailto:${worker.email}"))
                workerAtt.parameters.add(Role.REQ_PARTICIPANT)
                workerAtt.parameters.add(Cn("${worker.firstName} ${worker.lastName}"))

                // add the work shifts
                for (workShift in workerWorkTimes.value.workShifts) {
                    val durationStr = workShift.duration.toHours().toString() + ":" +
                            (workShift.duration.toMinutes() - (workShift.duration.toHours() * 60))
                    val eventName = "Work-Shift ${worker.firstName} ${worker.lastName} - $durationStr"
                    val start = workShift.from.toDateTime(timeZone.toZoneId())
                    val end = workShift.to.toDateTime(timeZone.toZoneId())
                    val vEvent = VEvent(start, end, eventName)
                    cal.components.add(vEvent)
                    vEvent.properties.add(workerAtt)
                }

                // add the day states
                for (dayState in workerWorkTimes.value.dayStates) {
                    val date = dayState.key
                    val dateStart = date.atStartOfDay()
                    val dateEnd = date.plusDays(1).atStartOfDay().minusSeconds(1)
                    val eventName = "Day states ${worker.firstName} ${worker.lastName} - ${dayState.value}"
                    val start = dateStart.toDateTime(timeZone.toZoneId())
                    val end = dateEnd.toDateTime(timeZone.toZoneId())
                    val vEvent = VEvent(start, end, eventName)
                    cal.components.add(vEvent)
                    vEvent.properties.add(workerAtt)
                    vEvent.properties.add(Categories(TextList(dayState.value.toList().map { it.name }.toTypedArray())))
                }
            }

            return cal
        }
    }

    override fun call() : SimulationResults {

        val workTimes: MutableMap<Worker, SimulationResult> = HashMap()
        for (worker in workers) {
            workTimes[worker] = simulate(worker)
        }

        return SimulationResults(workTimes)
    }

    private fun randomizeTime(hour: Int, minute: Int, steadinessFraction: Double) : LocalTime {

        val variance = COMMON_HOURS_OFF_SCHEDULE * (1.0 - steadinessFraction)
        val mean = LATENESS_BIAS
        val minuteOfTheDay = (hour + (random.nextGaussian() * variance + mean)) * 60 + minute
        var rndHour = floor(minuteOfTheDay / 60).toInt()
        val rndMinute = round(minuteOfTheDay).toInt() % 60
        if (rndHour >= 24) {
            rndHour = 23
        } else if (rndHour < 0) {
            rndHour = 0
        }
        return LocalTime.of(rndHour, rndMinute, 0, 0)
    }

    /**
     * @return the actual duration of the created shift
     */
    private fun addShift(
        workTimes: MutableList<WorkShift>,
        maxDuration: Duration,
        date: LocalDate,
        fromHour: Int,
        fromMinute: Int,
        toHour: Int,
        toMinute: Int,
        steadinessFraction: Double,
        remainingWork: Duration)
            : Duration
    {
        var duration = Duration.ZERO

        // try to generate a shift that does not exceed maxLength a few times,
        // but if those trials all fail, do not create a shift
        for (trials in 0..5) {
            val rndFrom = randomizeTime(fromHour, fromMinute, steadinessFraction)
            var rndTo = randomizeTime(toHour, toMinute, steadinessFraction)
            // we want work shifts to be at least 30 minutes long,
            // and also certainly not of negative length!
            while (rndTo.minusMinutes(30).isBefore(rndFrom)) {
                rndTo = randomizeTime(toHour, toMinute, steadinessFraction)
            }
            var trialDuration = Duration.between(rndFrom, rndTo)
            if (trialDuration > maxDuration) {
                continue
            }
            if (perfectEnding) {
                val upper = durationFromHours(PERFECT_TIME_UPPER_HOURS)
                if (trialDuration > remainingWork || trialDuration + upper >= remainingWork) {
                    rndTo = rndFrom + remainingWork;
                    trialDuration = Duration.between(rndFrom, rndTo)
                }
            }

            val from = date.at(rndFrom.hour, rndFrom.minute)
            val to = date.at(rndTo.hour, rndTo.minute)
            val shift = WorkShift(from, to)
            workTimes.add(shift)
            duration = trialDuration
            break
        }

        return duration
    }

    private fun simulate(worker: Worker) : SimulationResult {

        var simulationResult = SimulationResult(worker)
        for (employmentTime in worker.employmentTimes) {
            val periodInfo = DatePeriodInfo(employmentTime.from, employmentTime.to, worker.germanStateOfEmployment)
            simulationResult = simulationResult.combineWith(simulate(worker, periodInfo))
        }

        return simulationResult
    }

    private fun simulate(worker: Worker, datePeriodInfo: DatePeriodInfo) : SimulationResult {

        val workTimes = mutableListOf<WorkShift>()

        log.info("Simulating {} {} ...", worker.firstName, worker.lastName)

        val possibleWorkShifts = PossibleWorkShifts()
        if (worker.weeklyCoreTimes.isNotEmpty()) {
            possibleWorkShifts.addAll(worker.weeklyCoreTimes, SHIFT_LIKELIHOOD_CORE)
            possibleWorkShifts.addAll(DEFAULT_CORE_TIMES, SHIFT_LIKELIHOOD_DEFAULT)
        } else {
            log.info("No weekly core times supplied, using defaults")
            possibleWorkShifts.addAll(DEFAULT_CORE_TIMES, 1.0)
        }

        val totalVacationDays = (worker.yearlyVacationDays * datePeriodInfo.yearFraction).roundToInt()

        val totalWorkDays = datePeriodInfo.numWeekDays - datePeriodInfo.numNonWeekEndHolidays - totalVacationDays
        val totalRequiredWork = workDay
            .multipliedBy((totalWorkDays * worker.fullTimeFraction * 1000).roundToLong())
            .dividedBy(1000) // we do the 1000-thing for (hopefully) higher precision

        // number of days remaining in the period where one might possibly work
        var remainingWorkDays = totalWorkDays
        // number of hours the current worker still has to work this year
        var remainingWork = totalRequiredWork
        // number of days remaining in the period where one might possibly be on vacation
        var remainingVacationDays = totalVacationDays

        var daysOfWork = 0
        var sickDays = 0

        val weekDayPlanTmp =
            mutableMapOf<DayOfWeek, MutableList<ObjectLikelihood<WorkShiftPlan>>>()
        DayOfWeek.values().forEach { weekDayPlanTmp[it] = mutableListOf() }
        possibleWorkShifts.iterator().forEach { weekDayPlanTmp[it.obj.start.day]!!.add(it) }
        val weekDayPlan =
            weekDayPlanTmp.toMap<DayOfWeek, List<ObjectLikelihood<WorkShiftPlan>>>()

        // loop through all days of the year
        var curDate: LocalDate = datePeriodInfo.start
        var sick = false
        var vacation = false
        val dayStates = mutableMapOf<LocalDate, Set<DayState>>()
        while (curDate.isBefore(datePeriodInfo.end)) {
            val curDateStates = mutableSetOf<DayState>()
            var potentialWorkDay = true
            val sickLikelihood = worker.sicknessFraction +
                    (if (sick) SICKNESS_CONTINUATION_LIKELIHOOD else 0.0)
            var vacationLikelihood = remainingVacationDays.toDouble() / remainingWorkDays
            if (vacation && vacationLikelihood > 0.0) {
                vacationLikelihood = max(VACATION_CONTINUATION_LIKELIHOOD, vacationLikelihood)
            }
            vacation = (random.nextDouble() < vacationLikelihood)
            sick = (random.nextDouble() < sickLikelihood)

            // do not work on week-ends
            if (curDate.dayOfWeek == DayOfWeek.SATURDAY || curDate.dayOfWeek == DayOfWeek.SUNDAY) {
                curDateStates.add(DayState.WEEK_END)
                potentialWorkDay = false
            } else {
                curDateStates.add(DayState.WEEK_DAY)
            }
            // do not work on holidays
            if (datePeriodInfo.nonWeekEndHolidays.contains(curDate)) {
                curDateStates.add(DayState.HOLIDAY)
                potentialWorkDay = false
            }
            if (vacation) {
                curDateStates.add(DayState.VACATION)
                remainingVacationDays--
                potentialWorkDay = false
            }
            if (sick) {
                curDateStates.add(DayState.SICK)
                if (potentialWorkDay) {
                    sickDays++
                }
            }

            if (potentialWorkDay && !sick) {
                val curDatePlans = weekDayPlan[curDate.dayOfWeek] ?:
                        error("Failed to get work-shift plan for ${curDate.dayOfWeek}")

                // How big a fraction of a normal working day is the current day,
                // factoring in probability
                var curDateWorkLoad = 0.0
                curDatePlans.forEach { planLikelihood ->
                    curDateWorkLoad += planLikelihood.likelihood * planLikelihood.obj.duration().seconds / workDay.seconds
                }
                // decide whether this will be a work-day
                val remMinsOfWork = remainingWork.toMinutes()
                val remWorkTimeMins = remainingWorkDays * workDay.toMinutes()
                val workEverShift = remMinsOfWork * 1.1 > remWorkTimeMins
                val workDayLikelihood = if (remMinsOfWork == 0L) {
                    0.0
                } else if (workEverShift) {
                    1.0
                } else {
                    max(60.0, remMinsOfWork.toDouble()) /
                        remWorkTimeMins * 1.1 * curDateWorkLoad
                }
                if (random.nextBoolean(workDayLikelihood)) {
                    var worked = false
                    var maxLeftoverWorkToday = Duration.ofSeconds((MAX_WORK_DAY_HOURS * 60 * 60).roundToLong())
                    curDatePlans.forEach { plan ->
                        var planLikelihood = if (workEverShift) { 1.0 } else { plan.likelihood }
                        if (random.nextBoolean(planLikelihood)) {
                            val shiftDuration = addShift(
                                workTimes, maxLeftoverWorkToday, curDate,
                                plan.obj.start.time.hour, plan.obj.start.time.minute,
                                plan.obj.end.time.hour, plan.obj.end.time.minute,
                                worker.steadinessFraction, remainingWork
                            )

                            maxLeftoverWorkToday = maxLeftoverWorkToday.minus(shiftDuration)
                            remainingWork = remainingWork.minus(shiftDuration)
                            worked = true
                        }
                    }

                    if (worked) {
                        curDateStates.add(DayState.WORK)
                        daysOfWork++
                    }
                }
            }

            if (potentialWorkDay) {
                remainingWorkDays--
            }

            dayStates[curDate] = curDateStates
            curDate = curDate.plusDays(1)
        }

        if (log.isInfoEnabled) {
            // print out some statistics
            val totalActualWork = totalRequiredWork - remainingWork
            val actualWorkPercentage = fraction2Percent(totalActualWork.seconds.toDouble() / totalRequiredWork.seconds)
            log.info("    work-hours (actual / % / required): {} / {}% / {}",
                totalActualWork.toHumanString(), actualWorkPercentage, totalRequiredWork.toHumanString())

            val actualWorkDaysPercentage = fraction2Percent(daysOfWork.toDouble() / totalWorkDays)
            log.info("    work-days (actual / % / total): {}d / {}% / {}d",
                daysOfWork, actualWorkDaysPercentage, totalWorkDays)

            val sickDaysPercentage = fraction2Percent(sickDays.toDouble() / datePeriodInfo.numDays)
            log.info("    sick-days (actual / % / total): {}d / {}% / {}d",
                sickDays, sickDaysPercentage, datePeriodInfo.numDays)

            val vacationDays = totalVacationDays - remainingVacationDays
            val vacationDaysPercentage = fraction2Percent(vacationDays.toDouble() / datePeriodInfo.numDays)
            val targetVacationDaysPercentage = fraction2Percent(vacationDays.toDouble() / totalVacationDays)
            log.info("    vacation-days (actual / % / target% / total): {}d / {}% / {}% / {}d",
                vacationDays, vacationDaysPercentage, targetVacationDaysPercentage, datePeriodInfo.numDays)
        }

        return SimulationResult(worker, workTimes, dayStates.toMap())
    }
}
