package ganzlustig.attendancerecsim

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import java.io.File

class ParsedArgs(parser: ArgParser) {

    val workerParams : File? by parser.storing(
        "-w", "--workers",
        help = "path to the workers parameters file (format: YAML)") { File(this) }
        .default<File?>(null)

    val writeSampleWorkers : File? by parser.storing(
        "--sample-workers",
        help = "Write a sample workers file to the given path " +
                "(format: YAML)") { File(this) }
        .default<File?>(null)

    val iCalOutput by parser.storing(
        "-i", "--ical-file",
        help = "iCalendar output file path") { File(this) }
        .default(File("target/res.ical"))

    val noICalOutput by parser.flagging(
        "--no-ical",
        help = "Disable iCalendar output")

    val csvOutputWorkShifts by parser.storing(
        "--csv-out-shifts",
        help = "CSV output file path for work shifts") { File(this) }
        .default(File("target/res_shifts.csv"))

    val csvOutputDayStates by parser.storing(
        "--csv-out-states",
        help = "CSV output file path for workers day states") { File(this) }
        .default(File("target/res_states.csv"))

    val workDayHours by parser.storing(
        "--work-day-hours",
        help = "The fraction of hours in a full work-day (of which there are five in a regular work week)") { toDouble() }
        .default(WORK_DAY_HOURS)

    val seed by parser.storing(
        "--random-seed",
        help = "The seed for the random number generator; set this to a fixed value if you want reproducible results; leave it unspecified to use a random one") { toLong() }
        .default(System.currentTimeMillis()) // TODO Maybe rather use a non telling value here

    val perfectEnding by parser.flagging(
        "--perfect-ending",
        help = "End perfectly with the requested amount of hours. By default, the actual sum of working hours will just be more or less the requested amount.")
}
