package ganzlustig.attendancerecsim

import kotlinx.serialization.Serializable
import java.time.*
import java.time.format.DateTimeParseException

@Serializable
data class DatePeriod(@Serializable(with=LocalDateSerializer::class) val from: LocalDate, @Serializable(with=LocalDateSerializer::class) val to: LocalDate) {

    fun duration() : Period = from.until(to)

    constructor(from: LocalDate, duration: Period)
            : this(from, from.plus(duration))

    fun serialize() : List<String> {
        return listOf(
            from.toString(),
            duration().toString())
    }

    companion object {
        fun parse(parts: List<String>) : DatePeriod {
            return try {
                DatePeriod(
                    toLocalDate(parts[0]),
                    Period.parse(parts[1])
                )
            } catch (ex: DateTimeParseException) {
                DatePeriod(
                    toLocalDate(parts[0]),
                    toLocalDate(parts[1]))
            }
        }
    }
}
