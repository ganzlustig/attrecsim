package ganzlustig.attendancerecsim

import java.time.LocalDateTime

private class WorkShiftIterator(shiftsAndLikelihood: MutableList<ObjectLikelihood<WorkShiftPlan>>, start: LocalDateTime)
    : AbstractIterator<ObjectLikelihood<WorkShift>>()
{
    var curDateTime = start
    private val immutableShiftsAndLikelihood = shiftsAndLikelihood.toList() // NOTE I think this creates a deep copy of the list.. it should/has to!
    private var lastIndex = 0

    override fun computeNext() {

        if (immutableShiftsAndLikelihood.isEmpty()) {
            done()
        } else {
            val curWeekDayTime = curDateTime.toWeekDayTime()
            for (i in (0..immutableShiftsAndLikelihood.size)) {
                val curIndex = (lastIndex + i) % immutableShiftsAndLikelihood.size
                val curShiftAndLikelihood = immutableShiftsAndLikelihood[curIndex]
                val plan = curShiftAndLikelihood.obj
                if (plan.start.isAfter(curWeekDayTime)) {
                    setNext(ObjectLikelihood(WorkShift(plan.start.nextAfter(curDateTime), plan.duration()), curShiftAndLikelihood.likelihood))
                    return
                }
            }
            assert(false)
        }
    }
}

class PossibleWorkShifts : ArrayList<ObjectLikelihood<WorkShiftPlan>>() {

    fun addAll(shifts: List<WorkShiftPlan>, likelihood: Double) {
        shifts.forEach { add(ObjectLikelihood(it, likelihood)) }
    }

    fun iterator(start: LocalDateTime) : Iterator<ObjectLikelihood<WorkShift>> {
        return WorkShiftIterator(this, start)
    }
}
