# Attendance Recorder Simulation

This is a simple command-line tool that allows you to generate --
more-or-less realistic -- clock-in and -out time-stamps,
according to certain fixed input values
and using appropriate statistical distributions.

It is written in Kotlin, which runs on the Java Virtual Machine (JVM).

> NOTE:\
> You may want to use the
> [**latest build artifacts**](https://ganzlustig.gitlab.io/attrecsim/)
> in your own CI builds,\
> so you do not have to compile them each time from scratch.

## Compiling

You need [Maven 3](https://maven.apache.org/install.html) installed on your system.

On linux, this is usually the `maven` package.
So on Ubuntu or Debian, you can install it with:

```bash
apt-get install maven
```

Then compile this tool with:

```bash
mvn package
```

## Usage

To generate working times with this software, works like this:

1. [Generate a sample config file](#generate-the-config-file),
   using the software
2. Edit it, substituting the sample data with your actual workers data
3. [Generate time-stamps](#generate-time-stamps)
   by feeding the config file to the software

### Generate the config file

You can generate a sample config file to edit by hand afterwards:

```bash
java -jar target/*-jar-with-dependencies.jar \
     --sample-workers sampleWorkers.yml
```

Then edit the freshly generated _sampleWorkers.yml_ with a text editor,
preferably one with YAML syntax support.

**NOTE** \
You can leave out most of the fields in the config data,
which will activate the defaults.
Thus it is of course also possible,
to have any additional keys in the config file,
which will simply be ignored.

### Generate timestamps

while developing:

```bash
mvn exec:java -Dexec.args="--workers sampleWorkers.yml"
```

out there:

```bash
java -jar target/*-jar-with-dependencies.jar \
    --workers "$(pwd)sampleWorkers.yml"
```

You should now have a CSV table with the generated time-stamps in _target/shifts_res.csv_,
and an iCalendar file with the same info in _target/res.ical_.

**NOTE** \
If the generation hangs, and you are not in need of the iCal output,
use the additional switch `--no-ical`.
